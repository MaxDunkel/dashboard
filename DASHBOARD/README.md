# Dashboard

Ce projet a été réalisé dans le cadre de la formation Dev Web Javascript de Simplon, à Nanterre.

Il a pour but la création d'une application Dashboard, permettant à un enseignant de noter ses élèves, de signaler leurs retards et de partager ses commentaires. De leurs côtés, les élèves peuvent lire les commentaires et consulter leurs notes et leurs retards sous forme de graphs facilement compréhensibles.

## Progression

Travail en cours.

## Pour commencer

Ces instructions vous permettront d'obtenir une copie de notre projet et de la lancer à des fins de test et de développement. Référez vous à Déploiement pour lire les notes sur la méthode utilisée pour déployer le projet sur un système live.

### Prérecquis

```
• Nodejs
• vue-cli (npm install -g vue-cli)
• plugin Chrome pour Vue.js
```

### Installation

Une série d'exemples expliquant étape par étape comment faire tourner un environnement de développement.

Nommer l'étape

```
Donner l'exemple
```

bis repetita

```
Jusqu'à la fin
```
Finir avec une exemple montrant comment obtenir des données du système ou comment l'utiliser pour une petite demo.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Déploiement

Mettre ici les notes expliquant comment déployer ce projet sur un système live.

## Construit avec

* [Visual Studio Code](https://code.visualstudio.com/)
* [Material](https://material.io/)
* [Vue.js](https://fr.vuejs.org/)
* [Node.js](https://nodejs.org/en/)

## Auteurs

Fabien Grande (https://gitlab.com/Fabien.G)
Max Dunkel (https://gitlab.com/MaxDunkel)

Merci à tout le staff de Simplon, à Thomas, Junior et Kalidou pour leur soutien et leurs conseils.
